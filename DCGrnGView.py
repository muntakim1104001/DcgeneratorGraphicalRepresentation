import matplotlib.pyplot as plt
p=(0,40,50,90,110,120,130,145)
y=(5,80,120,160,180,185,195,195)
plt.plot(p,y,'r')
plt.title('Graphical representation of If vs Voc')
plt.xlabel('If(mA)')
plt.ylabel('Voc(V)')
plt.show()